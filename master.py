import socket 
import threading

HEADER = 64
PORT = 5050
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = "utf-8"
DISCONNECT_MESSAGE = "DISCONNECTED"
active_workers = []

#create a socket
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)

#handle EACH client
def handle_client(conn, addr):
	global active_workers
	print(f"[NEW CONNECTION] {addr} connected.")
	connected = True
	while connected:
		data = input("Input numbers: ")
		conn.send(data.encode(FORMAT))
		data_recv = conn.recv(1024).decode()
		print(data_recv)
		connected = False

	if connected == False:
		print(f"{addr} DISCONNECTED")

	active_workers.remove(addr)
	print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 2}: ", active_workers, "\n")
	conn.close()

#start the socket server, distribute the job
def start():
    #global active_workers
	server.listen()
	print(f"[LISTENING] Server is listening on {SERVER}")
	while True:
		conn, addr =  server.accept()
		thread = threading.Thread(target=handle_client, args=(conn, addr))
		thread.start()
		active_workers.append(addr)
		print(f"\n[ACTIVE CONNECTIONS] {threading.activeCount() - 1}: ", active_workers, "\n")


print("[STARTING] server is starting...")
start()