import socket

HEADER = 64
PORT = 5050
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "DISCONNECTED"
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(ADDR)

def sort_job(data):
	list_input = data.split()
	int_list = list(map(int, list_input))
	int_list.sort()
	listToStr = ' '.join(map(str, int_list))
	return listToStr

data = client.recv(1024).decode(FORMAT)

try:
	result = sort_job(data)
	print("DONE")
except ValueError:
	result = "Input harus berupa angka"

client.send(result.encode(FORMAT))

